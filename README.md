# dbt | tap-zendesk

## IMPORTANT: dbt Version Compatibility

The default branch on this repo uses `config-version=1`, and is maintained for compatibililty reasons with `dbt` versions `>=0.12.2 and <0.17`. For dbt versions `>=0.17`, we also maintain a `config-version-2` branch here in this repo. Please reference the `config-version-2` branch for all new projects. For more information on differences between the two formats, see the dbt documentation [here](https://docs.getdbt.com/docs/guides/migration-guide/upgrading-to-0-17-0).

## About this package

This [dbt](https://github.com/fishtown-analytics/dbt) package contains data models for data extracted from [Zendesk](https://developer.zendesk.com/rest_api) using [tap-zendesk](https://github.com/singer-io/tap-zendesk).

The models in this package are based for the most part on the [Zendesk models implemented by Fishtown Analytics](https://github.com/fishtown-analytics/zendesk).
