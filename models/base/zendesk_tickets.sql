with source as (
    
    select * from {{var('schema')}}.tickets
    
), 

renamed as (

    select
        --ids
        id as ticket_id,
        organization_id,
        assignee_id,
        brand_id,
        group_id,
        requester_id,
        submitter_id,
        
        --fields
        status,
        priority,
        subject,
        recipient,
        "type" as ticket_type,
        url as api_url,

        satisfaction_rating__id as satisfaction_rating_id,
        satisfaction_rating__score as satisfaction_rating_score,
        
        --dates
        EXTRACT(HOUR FROM created_at) created_hour,
        EXTRACT(DAY FROM created_at) created_day,
        EXTRACT(ISODOW FROM created_at) created_day_of_week,
        EXTRACT(WEEK FROM created_at) created_week,
        EXTRACT(MONTH FROM created_at) created_month,
        EXTRACT(QUARTER FROM created_at) created_quarter, 
        EXTRACT(YEAR FROM created_at) created_year,
                
        created_at,
        updated_at
        
    from source
    
)

select * from renamed